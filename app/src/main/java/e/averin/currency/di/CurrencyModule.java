package e.averin.currency.di;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by finky on 04-May-17.
 */

@Module
public class CurrencyModule {
    @Provides
    @CurrencyUrl
    String providesCurrencyUrl() {
        return "https://alfabank.ru/currency";
    }

    @Provides
    Scheduler provideMainThread() {
        return AndroidSchedulers.mainThread();
    }
}
