package e.averin.currency.di;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Component;
import e.averin.currency.main_screen.CurrencyActivity;

/**
 * Created by finky on 04-May-17.
 */
@Component(modules = {CurrencyModule.class, BindsModule.class})
@Singleton
public interface CurrencyComponent {
    void inject(CurrencyActivity activity);
}
