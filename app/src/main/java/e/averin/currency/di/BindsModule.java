package e.averin.currency.di;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import e.averin.currency.main_screen.presenter.CurrencyPresenter;
import e.averin.currency.main_screen.presenter.ICurrencyPresenter;

/**
 * Created by finky on 04-May-17.
 */

@Module
public abstract class BindsModule {
    @Binds
    @Singleton
    public abstract ICurrencyPresenter bindsCurrencyPresenter(CurrencyPresenter presenter);
}
