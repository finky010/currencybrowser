package e.averin.currency;

import android.app.Application;

import e.averin.currency.di.CurrencyComponent;
import e.averin.currency.di.DaggerCurrencyComponent;

/**
 * Created by finky on 04-May-17.
 */

public class CurrencyApp extends Application {
    private static CurrencyComponent component;

    public static CurrencyComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent();
    }

    private CurrencyComponent buildComponent() {
        return DaggerCurrencyComponent.create();
    }
}
