package e.averin.currency.main_screen;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import e.averin.currency.CurrencyApp;
import e.averin.currency.R;
import e.averin.currency.main_screen.presenter.ICurrencyPresenter;
import e.averin.currency.main_screen.view.ICurrencyView;

public class CurrencyActivity extends MvpActivity<ICurrencyView, ICurrencyPresenter> implements ICurrencyView {

    @Inject
    ICurrencyPresenter currencyPresenter;

    private WebView currencyBrowser;
    private TextView euro;
    private TextView usd;
    private TextView timestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CurrencyApp.getComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);

        currencyBrowser = (WebView) findViewById(R.id.webview);
        initBrowser();
        usd = (TextView) findViewById(R.id.usd);
        euro = (TextView) findViewById(R.id.eur);
        timestamp = (TextView) findViewById(R.id.timestamp);

        presenter.viewCreated();
    }

    private void initBrowser() {
        currencyBrowser.getSettings().setJavaScriptEnabled(true);
        currencyBrowser.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return presenter.shouldOverrideUrlLoading(url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                presenter.onPageFinished();
            }
        });
        currencyBrowser.addJavascriptInterface(this, "HTMLOUT");
    }

    @NonNull
    @Override
    public ICurrencyPresenter createPresenter() {
        return currencyPresenter;
    }

    @Override
    public void loadUrl(String url) {
        currencyBrowser.loadUrl(url);
    }

    @Override
    public void getUsdRateFromBrowser() {
        currencyBrowser.loadUrl("javascript:window.HTMLOUT.getUsdRate(document.body.getElementsByClassName(\"tr usd\")[0].innerText);");
    }

    @Override
    public void getEuroRateFromBrowser() {
        currencyBrowser.loadUrl("javascript:window.HTMLOUT.getEuroRate(document.body.getElementsByClassName(\"tr eur\")[0].innerText);");
    }

    @Override
    public void showUsdRates(String rates) {
        usd.setText(rates);
    }

    @Override
    public void showEuroRates(String rates) {
        euro.setText(rates);
    }

    @Override
    public void showTimeStamp(long time) {
        java.util.Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        timestamp.setText(sdf.format(date));
    }

    @JavascriptInterface
    @SuppressWarnings("unused")
    public void getUsdRate(final String usd) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                presenter.onUsdRateReturned(usd);
            }
        });
    }

    @JavascriptInterface
    @SuppressWarnings("unused")
    public void getEuroRate(final String euro) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                presenter.onEuroRateReturned(euro);
            }
        });
    }
}
