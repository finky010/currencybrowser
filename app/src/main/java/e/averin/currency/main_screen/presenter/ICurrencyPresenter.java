package e.averin.currency.main_screen.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import e.averin.currency.main_screen.view.ICurrencyView;

/**
 * Created by finky on 03-May-17.
 */

public interface ICurrencyPresenter extends MvpPresenter<ICurrencyView> {
    void viewCreated();

    boolean shouldOverrideUrlLoading(String url);

    void onPageFinished();

    void onUsdRateReturned(String usd);

    void onEuroRateReturned(String euro);
}
