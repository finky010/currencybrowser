package e.averin.currency.main_screen.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import e.averin.currency.di.CurrencyUrl;
import e.averin.currency.main_screen.view.ICurrencyView;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by finky on 04-May-17.
 */

public class CurrencyPresenter extends MvpBasePresenter<ICurrencyView> implements ICurrencyPresenter {

    public static final int REFRESH_TIME_MINUTES = 1;
    private String currencyUrl;
    private Disposable timer;
    private Scheduler mainThread;

    @Inject
    public CurrencyPresenter(@CurrencyUrl String url, Scheduler mainThread) {
        this.currencyUrl = url;
        this.mainThread = mainThread;
    }

    @Override
    public void viewCreated() {
        if (isViewAttached()) {
            getView().loadUrl(currencyUrl);
            if (timer != null && !timer.isDisposed()) {
                timer.dispose();
            }
            createTimerThread();
        }
    }

    @Override
    public boolean shouldOverrideUrlLoading(String url) {
        if (isViewAttached()) {
            getView().loadUrl(url);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onPageFinished() {
        if (isViewAttached()) {
            getView().getUsdRateFromBrowser();
            getView().getEuroRateFromBrowser();
        }
    }

    @Override
    public void onUsdRateReturned(String usd) {
        if(!usd.isEmpty()) {
            if (isViewAttached()) {
                getView().showUsdRates(usd);
                getView().showTimeStamp(System.currentTimeMillis());
            }
        }
    }

    @Override
    public void onEuroRateReturned(String euro) {
        if (!euro.isEmpty()) {
            if (isViewAttached()) {
                getView().showEuroRates(euro);
            }
        }
    }

    private void createTimerThread() {
        //operates on Scheduler.computation() by default
        timer = Observable.interval(REFRESH_TIME_MINUTES, REFRESH_TIME_MINUTES, TimeUnit.MINUTES)
                .observeOn(mainThread)
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        onPageFinished();
                    }
                });
    }
}
