package e.averin.currency.main_screen.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by finky on 03-May-17.
 */

public interface ICurrencyView extends MvpView {
    void loadUrl(String url);

    void getUsdRateFromBrowser();

    void getEuroRateFromBrowser();

    void showUsdRates(String rates);

    void showEuroRates(String rates);

    void showTimeStamp(long time);
}
